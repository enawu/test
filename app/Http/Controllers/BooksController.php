<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Book;
use App\BookReview;
use App\Http\Requests\PostBookRequest;
use App\Http\Requests\PostBookReviewRequest;
use App\Http\Resources\BookResource;
use App\Http\Resources\BookReviewResource;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function getCollection(Request $request)
    {
        // @TODO implement
        $list = Book::withAvg(['reviews','authors'])->orderBy('title', 'asc','revievs_avg','asc')->paginate(25);

        $list = $list->sortByAsc('title');

        Hackathon::withAvg('participants')->orderBy('participants_count', 'desc')->paginate(10);

        return BookResource::collection(Book::with('ratings')->paginate(25));
        // return BookResource::collection();
    }

    public function post(PostBookRequest $request)
    {
        $book = new Book();

        // @TODO implement

        return new BookResource($book);
    }

    public function postReview(int $bookId, PostBookReviewRequest $request)
    {
        $bookReview = new BookReview();

        // @TODO implement

        return new BookReviewResource($bookReview);
    }
}
